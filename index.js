let getCube = function(number){
	cube = number**3;
	console.log(`The cube of ${number} is ${cube}.`);
}

getCube(2);

let address = ["258 Washington Ave NW", "California", "90011"];
const [streetName, stateName, zipCode] = address;
console.log(`I live at ${streetName}, ${stateName}, ${zipCode}.`)

let animal = {
	name: "Lolong",
	typeAnimal: "saltwater crocodile",
	weight: "1075 kgs",
	lenght: "20 ft 3 in"
}

const{name, typeAnimal, weight, lenght} = animal;
console.log(`${name} was a ${typeAnimal}. He weighed ${weight} with a measurement of ${lenght}.` )

let arrayNum = [1, 2, 3, 4, 5];
arrayNum.forEach((num) => console.log(`${num}`));

let reduceNumber = arrayNum.reduce((acc, cur) => acc + cur);
console.log(reduceNumber);


class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog();
myDog.name = "Frankie";
myDog.age = 5;
myDog.breed = "Miniature Dachshund";
 console.log(myDog);
